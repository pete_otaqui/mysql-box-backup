#!/bin/bash

source $DIR/config.sh


mysqldump -u $MYSQL_USERNAME -p$MYSQL_PASSWORD $MYSQL_DATABASE > $DIR/$FILENAME

gzip $DIR/$FILENAME
