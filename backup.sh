#!/bin/bash

echo "running box mailer `date`"
export DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

source $DIR/config.sh

$DIR/dump.sh

$DIR/send.sh

