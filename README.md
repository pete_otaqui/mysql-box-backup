MySQL Box Backup
================

This set of scripts makes it easy to backup a MySQL database to Box.com

Setup
-----

First of all do the following:

* Sign up for Mailgun, and get your API key and "send from" domain ready
* Sign up for Box.com, create a target folder, allow emails to send attachments to this folder, get that email address ready

Installation
------------

* Copy `config.example.sh` to `config.sh` and enter your API key, box.com email address and other values
* Test your setup by running `./backup.sh`
* Add a crob job to run `backup.sh` as often you need

