#!/bin/bash

# "config.sh" should contain API_KEY and SEND_TO
source $DIR/config.sh

curl -s --user $MG_API_KEY \
    $MG_ENDPOINT \
    -F from=$MG_SEND_FROM \
    -F to=$SEND_TO \
    -F subject=$SUBJECT \
    -F text=$TEXT \
    -F attachment=@$DIR/$FILENAMEGZ

